/* global document, Office */

Office.onReady(function() {
  console.log("READy");
  setup();

  document.getElementById("taskId").addEventListener("keypress", function(e) {
    if (e.key == "Enter") {
      var taskId = document.getElementById("taskId").value;
      showTaskInfo(taskId);
    }
  });
});

async function setup() {
  checkLogin();
  listTasks();
}

async function checkLogin() {
  var now = Date.now();
  var expireDate = localStorage.getItem("expireDate");
  if (localStorage.getItem("access_token") == null || localStorage.getItem("access_token") == "undefined" ||  now > expireDate) {
    window.location.replace("https://officeadd-in.bitbucket.io/src/dialog/myDialog.html");
  }
}

async function listTasks() {
  checkLogin();

  var access_token = localStorage.getItem("access_token");
  const response = await fetch(`https://api.rambase.net/collaboration/tasks?$access_token=${access_token}`);
  const tasks = await response.text().then(data => {
    return data;
  });

  await parseTasks(tasks);

  document.getElementById("tasks").style.visibility = "visible";

  var lis = document.getElementById("taskList").getElementsByTagName("a");
  for (let i = 0; i < lis.length; i++) {
    lis[i].addEventListener("click", function() {
      showTaskInfo(this.id);
    });
  }
}

async function showTaskInfo(taskId) {
  checkLogin();

  var access_token = localStorage.getItem("access_token");
  const response = await fetch(`https://api.rambase.net/collaboration/tasks/${taskId}?$access_token=${access_token}`);
  const task = await response.text().then(data => {
    return data;
  });

  const iframeDocument = document.getElementById("taskInfoField").contentDocument;
  iframeDocument.open("text/html", "replace");
  iframeDocument.write(task);
  iframeDocument.close();

  //document.getElementById("taskInfoField").src = "data:text/html;charset=UTF-8," + "<html><body><p>hello!</p></body></html>";
}

// Parsing the response body html to get the task-id and task-title fields.
async function parseTasks(tasks) {
  document.getElementById("taskList").innerHTML = "";

  var doctype = document.implementation.createDocumentType("html", "", "");
  var dom = document.implementation.createHTMLDocument("", "html", doctype);

  dom.documentElement.innerHTML = tasks;

  var taskListHtml = dom.documentElement.lastElementChild.childNodes[3].childNodes[3].childNodes;

  taskListHtml.forEach(taskDiv => {
    if (taskDiv.length != 1) {
      var taskEntryId = taskDiv.firstChild.childNodes[3].childNodes[1];
      var taskEntryTitle = taskDiv.firstChild.childNodes[3].childNodes[3];

      const taskId = taskEntryId.childNodes[2].innerHTML;
      const taskTitle = taskEntryTitle.childNodes[2].innerHTML;

      var listItem = document.createElement("li");
      listItem.innerHTML = taskId + ":  " + taskTitle;

      var listItemWrap = document.createElement("a");
      listItemWrap.className = "clickText";
      listItemWrap.id = taskId;

      document.getElementById("taskList").appendChild(listItemWrap);

      listItemWrap.appendChild(listItem);
    }
  });
}
