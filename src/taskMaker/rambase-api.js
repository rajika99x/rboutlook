/* global document, Office */

Office.onReady(function() {
  setup();

  document.getElementById("confirm").addEventListener("click", function() {
    confirmTask();
  });

  document.getElementById("logout").addEventListener("click", function() {
    logout();
  });

  document.getElementById("taskId").addEventListener("keypress", function(e) {
    if (e.key == "Enter") {
      var taskId = document.getElementById("taskId").value;
      showTaskInfo(taskId);
    }
  });
});

async function setup() {
  listTasks();
  createTask();
}

async function listTasks() {
  var access_token = localStorage.getItem("access_token");

  catchList().catch(error => {
    window.location.replace("https://localhost:3000/src/dialog/myDialog.html");
  });

  async function catchList() {
    const response = await fetch(`https://api.rambase.net/collaboration/tasks?$access_token=${access_token}`);

    if (response.status === 401) {
      throw Error("not logged in");
    } else {
      const tasks = await response.text().then(data => {
        return data;
      });

      await parseTasks(tasks);

      document.getElementById("tasks").style.visibility = "visible";

      var lis = document.getElementById("taskList").getElementsByTagName("a");
      for (let i = 0; i < lis.length; i++) {
        lis[i].addEventListener("click", function() {
          showTaskInfo(this.id);
        });
      }
    }
  }
}

async function showTaskInfo(taskId) {
  var access_token = localStorage.getItem("access_token");
  const response = await fetch(`https://api.rambase.net/collaboration/tasks/${taskId}?$access_token=${access_token}`);
  const task = await response.text().then(data => {
    return data;
  });

  const iframeDocument = document.getElementById("taskInfoField").contentDocument;
  iframeDocument.open("text/html", "replace");
  iframeDocument.write(task);
  iframeDocument.close();

  //document.getElementById("taskInfoField").src = "data:text/html;charset=UTF-8," + "<html><body><p>hello!</p></body></html>";
}

async function createTask() {
  const title = localStorage.getItem("title");
  const description = localStorage.getItem("description");

  document.getElementById("title").value = title;
  document.getElementById("description").innerHTML = description;
}

async function confirmTask() {
  var access_token = localStorage.getItem("access_token");

  const title = document.getElementById("title").value;
  const description = document.getElementById("description").innerHTML;
  const priority = document.getElementById("priority").value;
  const isPrivate = document.getElementById("private").value;
  const deadline = document.getElementById("deadline").value;
  const activity = document.getElementById("activity").value;
  const assignedTo = document.getElementById("assignedTo").value;
  const processId = document.getElementById("processId").value;

  var dateTime = new Date(deadline);

  const response = await fetch(`https://api.rambase.net/collaboration/tasks?$access_token=${access_token}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      task: {
        title: title,
        description: description,
        priority: priority,
        isPrivate: isPrivate,
        deadlineAt: dateTime,
        activity: activity,
        assignedTo: { employeeId: assignedTo },
        process: { processId: processId }
      }
    })
  });
}

async function parseTasks(tasks) {
  document.getElementById("taskList").innerHTML = "";

  var doctype = document.implementation.createDocumentType("html", "", "");
  var dom = document.implementation.createHTMLDocument("", "html", doctype);

  dom.documentElement.innerHTML = tasks;

  var taskListHtml = dom.documentElement.lastElementChild.childNodes[3].childNodes[3].childNodes;

  taskListHtml.forEach(taskDiv => {
    if (taskDiv.length != 1) {
      var taskEntryId = taskDiv.firstChild.childNodes[3].childNodes[1];
      var taskEntryTitle = taskDiv.firstChild.childNodes[3].childNodes[3];

      const taskId = taskEntryId.childNodes[2].innerHTML;
      const taskTitle = taskEntryTitle.childNodes[2].innerHTML;

      var listItem = document.createElement("li");
      listItem.innerHTML = taskId + ":  " + taskTitle;

      var listItemWrap = document.createElement("a");
      listItemWrap.className = "clickText";
      listItemWrap.id = taskId;

      document.getElementById("taskList").appendChild(listItemWrap);

      listItemWrap.appendChild(listItem);
    }
  });
}

function logout() {
  fetch("https://api.rambase.net/oauth2/logout/", { method: "POST" });
  localStorage.removeItem("access_token");
  window.location.reload();
}
