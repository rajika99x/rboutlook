/* global Office */

Office.onReady(function() {
  const url = new URL(window.location.href);
  const code = url.searchParams.get("code");
  getAccesstoken(code);
});

async function getAccesstoken(code) {
  const response = await fetch("https://api.rambase.net/oauth2/access_token/", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body: `code=${code}&client_id=YFvZjUr4tk-J7tbHn-rcjg2&client_secret=7N7T1bYqdkKRELeW6CBIrw2&redirect_uri=https://officeadd-in.bitbucket.io/src/dialog/redirect.html&grant_type=authorization_code`
  });

  var expireTime;

  const accesstoken = await response.json().then(data => {
    expireTime = data.expires_in;
    return data.access_token;
  });

  var now = new Date(Date.now());
  var expireDate = new Date(now.getTime() + (expireTime*1000)).getTime();

  localStorage.setItem("expireDate", expireDate);

  localStorage.setItem("access_token", accesstoken);

  var url = localStorage.getItem("url");

  if (url != "https://officeadd-in.bitbucket.io/src/dialog/myDialog.html") {
    window.location.replace(url);
  } else {
    Office.context.ui.messageParent("logged in");
  }
}
