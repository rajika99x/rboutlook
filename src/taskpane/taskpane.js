/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

/* global document, Office */

const loginurl = "https://officeadd-in.bitbucket.io/src/dialog/myDialog.html";
const infourl = "https://officeadd-in.bitbucket.io/src/taskMaker/taskInfo.html";

Office.onReady(info => {
  console.log(info, "OnReady");
  if (info.host === Office.HostType.Outlook) {
    Office.context.tab
    //Office.context.ui.openBrowserWindow("https://rambase.net");

    document.getElementById("sideload-msg").style.display = "none";
    document.getElementById("app-body").style.display = "flex";

    checkLogin();

    //  Checks for existing task that has not yet been created and creates it.
    if (localStorage.getItem("prevRequestBody") != null) {
      confirmTask(localStorage.getItem("prevRequestBody"));
      localStorage.removeItem("prevRequestBody");
    }

    // localStorage.setItem("title", Office.context.mailbox.item.subject);
    // Office.context.mailbox.item.body.getAsync(
    //   "text",
    //   { asyncContext: "This is passed to the callback" },
    //   function callback(result) {
    //     localStorage.setItem("description", result.value);
    //   }
    // );

    setup();

    document.getElementById("confirm").addEventListener("click", function() {
      confirmTask();
    });

    document.getElementById("logout").addEventListener("click", function() {
      logout();
    });

    document.getElementById("task_info").addEventListener("click", function() {
      run(infourl);
    });
  }
});

async function checkLogin() {
  var now = Date.now();
  var expireDate = localStorage.getItem("expireDate");

  if (localStorage.getItem("access_token") == null || localStorage.getItem("access_token") == "undefined" ||  now > expireDate) {
    run(loginurl);
  }
}

async function run(url) {
  localStorage.setItem("url", url);

  var dialog;
  Office.context.ui.displayDialogAsync(url, { height: 80, width: 80 }, function(asyncResult) {
    dialog = asyncResult.value;
    dialog.addEventHandler(Office.EventType.DialogMessageReceived, processMessage);
  });

  async function processMessage(arg) {
    dialog.close();
  }
}

async function setup() {
  document.getElementById("title").value = Office.context.mailbox.item.subject;

  Office.context.mailbox.item.body.getAsync("text", function callback(result) {
    document.getElementById("description").innerHTML = result.value;
  });
}

async function confirmTask(requestBody = null) {
  var access_token = localStorage.getItem("access_token");

  const title = document.getElementById("title").value;
  const description = document.getElementById("description").innerHTML;
  const priority = document.getElementById("priority").value;
  const isPrivate = document.getElementById("private").value;
  const deadline = document.getElementById("deadline").value;
  const activity = document.getElementById("activity").value;
  const assignedTo = document.getElementById("assignedTo").value;
  const processId = document.getElementById("processId").value;

  var dateTime = new Date(deadline);

  if (requestBody == null) {
    requestBody = JSON.stringify({
      task: {
        title: title,
        description: description,
        priority: priority,
        isPrivate: isPrivate,
        deadlineAt: dateTime,
        activity: activity,
        assignedTo: { employeeId: assignedTo },
        process: { processId: processId }
      }
    });
  }

  const response = await fetch(`https://api.rambase.net/collaboration/tasks?$access_token=${access_token}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: requestBody
  }).catch(function() {
    localStorage.setItem("prevRequestBody", requestBody);
  });
}

function logout() {
  fetch("https://api.rambase.net/oauth2/logout", { method: "POST" });
  localStorage.removeItem("access_token");
  Office.context.ui.closeContainer();
}
