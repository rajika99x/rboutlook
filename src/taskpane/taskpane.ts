/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

import { error } from "jquery";

/* global document, Office */

Office.onReady(info => {
  console.log("HERE");
  if (info.host === Office.HostType.Outlook) {
    console.log(info);
    document.getElementById("sideload-msg").style.display = "none";
    document.getElementById("app-body").style.display = "flex";
    
    var taskurl = 'https://rambase.net';

    localStorage.setItem("title", Office.context.mailbox.item.subject);
    Office.context.mailbox.item.body.getAsync(
      "text",
      { asyncContext: "This is passed to the callback" },
      function callback(result) {
        localStorage.setItem("description", result.value);
      });

    document.getElementById("create_task").addEventListener("click", function () { run(taskurl) });
  }
});

export async function run(url) {

  var dialog;
  Office.context.ui.displayDialogAsync(url, { width: 80, height: 85 },
    function (asyncResult) {
      dialog = asyncResult.value;
      dialog.addEventHandler(Office.EventType.DialogMessageReceived, processMessage);
    })


  async function processMessage(arg) {
    dialog.close();
  }
}
